%define SIZE 8
%include "lib.inc"
;rsi - *begginning of the dict, rdi - *terminator
global find_word
section .text
find_word:
	.iterate:
		cmp rsi, 0
		jz .false
		push rdi
		push rsi
		add rsi, SIZE
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 1
		jz .true
		mov rsi, [rsi]
		jmp .iterate
		
	.false:
		xor rax, rax
		ret
		
	.true:
		mov rax, rsi
		ret
	