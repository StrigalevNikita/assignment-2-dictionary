extern find_word
global _start
%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%define SIZE 8
%define BUFFER 255

section .rodata
	type_word: db "Word input", 0
	exception_input: db "Exception in input found (overflow)", 0
	answer_word: db "Answer: ", 0
	exception_dict: db "Exception in find word (word not found)", 0
	

section .bss
	buff: resb BUFFER
	

section .text
	_start:
		mov rdi, type_word
		call print_string
		call print_newline
		mov rsi, BUFFER
		mov rdi, buff
		call string_read
		cmp rax, 0
		jz .exception_input_found
		mov rdi, rax
		mov rsi, first_word
		call find_word
		cmp rax, 0
		jz .exception_word_found
		mov rdi, answer_word
		push rax
		call print_string
		pop rax
		add rax, SIZE
		mov rdi, rax
		push rdi
		call string_length
		pop rdi
		add rax, 1
		add rdi, rax
		call print_string
		jmp .end_programm
	.exception_input_found:
		mov rdi, exception_input
		call print_exception
		jmp .end_programm
	.exception_word_found:
		mov rdi, exception_dict
		call print_exception
	.end_programm:
		jmp exit
	
