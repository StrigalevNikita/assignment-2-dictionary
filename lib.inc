; import io functions from lib.asm
extern string_copy
extern parse_int
extern parse_uint
extern read_word
extern read_char
extern string_equals
extern print_int
extern print_uint
extern exit
extern string_length
extern print_string
extern print_char
extern print_newline
extern print_exception
extern string_read
