lab: lib.o dict.o main.o
	ld -o $@ $^
lib.o: lib.asm
	nasm -felf64 -o $@ $<
dict.o: dict.asm
	nasm -felf64 -o $@ $<
main.o: main.asm
	nasm -felf64 -o $@ $<
.PHONY: clean
clean:
	rm -rf *.o
